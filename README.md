No longer maintained.

# KEX - Library for easier management and result representation of Keras experiments.

Author: paloha<br>
Acknowledgement: The development of this software was supported by Brno University of Technology, Czechia.

This library simplifies the workflow of training multiple DNN models using
Keras and Theano frameworks and automatically presents the results. 
It is very useful for hyperparameter tuning when you need to train 
lots of models to find the best performing one.

Kex will wrap all defined models into one experiment, train them and
save all the configuration files and results into one folder, so you can
easily compare which model performed better.


![steps](/uploads/bbe974d3051a253e2401268ba8e81710/steps.png "Simplified KEX workflow.")

## Platform and Dependencies
Tested on:
* Ubuntu 14.04 or 16.04, so far does not work on Windows
* Python 3.4, will also work with higher versions with slight changes

Needs:
* working Keras < 2.0 on Theano
* numpy, texttable, json, matplotlib, sklearn, h5py


## Main features
1. Configuration of all models resides in just one file, so you can configure all models at once easily.
2. Uses data generator as input, so it can handle large amounts of data.
3. Data preparation is done beforehand on CPU and training on GPU, so only the speed of your GPU is the bottleneck.
4. Utilizes multiple threads to run models on multiple GPUs, so you can train multiple models at once. <br> 
WARNING: it does not spread one model over multiple GPUs, it just spreads multiple models over multiple GPUs.
5. It handles GPU Memory errors. If you, by mistake, define a model that won't fit in your GPU memory, 
the program won't crash and continues to train the next model, so you won't have to start over.
6. It can resume the experiment after marginal crash such as machine shutdown etc., so you won't have to start over.
5. It assures reproducible results if the model definition and initial weights did not change.

## What is in this repository?
* ```datagenerator.py.template``` - a template on which you should build your own datagenerator.
* ```model.py.template``` - a template on which you should build your own experiment configuration.
* ```experiment.py``` - the code that handles training of one model and the outputting of the results.
* ```run.py``` - the code that queues the models and runs the ```experiment.py``` on free GPU core. 
* ```.gitignore``` - file that specifies that folders ```/Auxiliar``` and ```/Exp``` 
and files ```model.py``` and ```datagenerator.py``` are not pushed into repository. That means 
use the ```/Auxiliar``` fodler to store your data and other auxiliar stuff.

## Input and Output
**INPUT:** 

User defined data generator in ```datagenerator.py``` that yields data 
batch by batch in an endless loop (based on provided template in ```datagenerator.py.template```).

\* NOTE that if you will need to resume your experiment, your datagenerator will be freshly invoked. 
So be sure it always generates the same data in the same order, 
otherwise your train / validation / testing sets might get mixed up. That would wrongly boost your 
validation and testing accuracy.

**OUTPUT:**

As a result you will get a folder ```Exp-nnnnn``` in project root containing a separate folder ```Model-xxxx```
for each of your models. 

If the training of the model was successful, model folder is renamed  to contain quick references 
to validation and training accuracies and number of best epoch e.g. ```V99.99-T98.70-BE152_Model-1003```.
The ```nnnnn``` in the experiment folder name represents a random identification number of the experiment.
The ```xxxx``` in the model folder name represents the ordinal number of the model + 1000, starts from 0.
So if you have defined 3 models in your ```model.py```, they had been denoted with ```1000, 1001 and 1002```.

![expfolder](/uploads/a31d36f3a70c0e5d0dfb427fff1d1236/expfolder.png  "Example of the experiment folder structure.")

Each model's folder will contain: 
1. ```xxxx_exp_code.py``` - copy of the model code, so even after a long time you can 
reproduce the exact same model.
2. ```xxxx-mod_yy.h5``` - last trained model, so you can resume training were you have left of.
3. ```xxxx-mod_best.h5``` - best model based on validation accuracy, so you can use it for predictions.
4. ```xxxx-mod_plot.png``` - plot of the model architecture, so you can quickly see the layers.
5. ```xxxx_mod_summ.txt``` - text file with model summary, so you can see the parameters of each 
layer and other details.
6. ```xxxx_res_log.txt``` - text file with results of predictions on validation and testing files.
7. ```xxxx-run_hist.json``` - json with running history of training for each epoch containing
lists of 'acc', 'val_acc' etc. updated at the end of each epoch (necessary for restarting the model.)
8. ```xxxx-tva_plot.pdf``` - plot of training and validation accuracy over number of epochs, 
so you can see if you model starts to overfit and where.
9. ```xxxx-tvh_dict.json``` - same as running history, but saved after all the training has finished.
10. ```xxxx_tvh_log.txt``` - textfile containing all the important results as confusion matrices, 
classification reports etc. ```***``` denotes the best epoch from the list of all epochs.


![results](/uploads/2c159a89883a24627ebd74f947b031a3/results.png "Example of the results of one DNN model.")

## Usage

**BASIC USAGE:**
1. Clone the repository with ```$ git clone https://gitlab.com/paloha/kex```
2. Copy the ```datagenerator.py.template```, rename it to ```datagenerator.py``` and rewrite if for your own data.
3. Copy the ```model.py.template```, rename it to ```model.py``` and rewrite it for your own DNN architecture and hyperparameters.
4. Run the ```run.py``` with ```$ python3 run.py```, progress will be printed to the console
5. After the training open the freshly created folder ```Exp/Exp-nnnnn``` to see the results.

**RESUMING AFTER MACHINE CRASH:**

1. Just edit ```run.py``` and pass two parameters ```resume=True``` and 
```exp_id_to_resume=nnnnn``` to ```bootstrap_the_experiment()``` function. 
```nnnnn``` in ths case is the ID of the experiment you want to resume.
<br><br>
\* NOTE that if you resume your experiment, your datagenerator is freshly invoked. 
So be sure it always generates the same data in the same order, 
otherwise your train / validation / testing sets might get mixed up. That would wrongly boost your 
validation and testing accuracy.

**TRAIN ALREADY TRAINED MODEL WITH ADDITIONAL EPOCHS:**

\* This is useful when your model reached configured number of epochs whilst still improoving. 
\* Basically you have to create a new imaginary experiment with all the models you want to train 
and pretend that machine just crashed.

1. Create new folder ```Exp/Exp-zzzzz``` where ```zzzzz``` is a random 5 digit number chosen by you.
2. Copy one or more model folders e.g. ```V98.65-T98.70-BE152_Model-1003``` that you want to train with additional epochs.
3. Rename the model folders back to this format ```Model-xxxx```.
4. Edit the ```xxxx_exp_code.py``` of one of the models so the ```tryparameters``` list contains
dictionaries of parameters for all the models. The order of the dicts is important. 
First is for the model 1000, secont for 1001 etc. Rename the model folder's ```xxxx``` and model file's ```xxxx``` accordingly.
5. Change the number of epochs in the ```xxxx_exp_code.py``` to the desired amount.
6. Remove the last 3 lines from the ```xxxx_exp_code.py``` that contains comment of used parameters.
7. Copy this refactored ```xxxx_exp_code.py``` to every model folder in this new ```Exp-zzzzz```.
8. Remove all the files from all the model folders that are being created at the end of the training. 
Delete all these: ```xxxx_tvh_log.txt```, ```xxxx-tvh_dict.json```, ```xxxx-tva_plot.pdf```, ```xxxx_res_log.txt```, ```xxxx-mod_best.h5```.
9. Double check if all the ```xxxx``` IDs of all the files in the model's folder matches 
the model's ID ```xxxx```. If not, rename accordingly.
10. Finally follow the instructions in the section **RESUMING AFTER MACHINE CRASH**.

## License
This project is licensed under the terms of the MIT license.