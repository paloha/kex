##############################################################################
# This is a set of functions called in run() that runs the DNN training
# It saves all necessary information about training and results to various files
##############################################################################

import numpy as np

np.random.seed(2004)
import texttable as tt
import os, sys, json, time
from shutil import copyfile
import matplotlib.pyplot as plt
from datagenerator import data_provide
from sklearn.metrics import classification_report, confusion_matrix

from keras.models import load_model
from keras.utils.visualize_util import plot as kplot
from keras.callbacks import Callback, ModelCheckpoint, LambdaCallback
from keras.utils.np_utils import categorical_probas_to_classes


def loadmodelmodul(modelpath):
    # Importing model from its path // for Python 3.3 & 3.4
    from importlib.machinery import SourceFileLoader
    model = SourceFileLoader("model.py", modelpath).load_module()

    # Importing model from its path // for Python 3.5
    # import importlib.util
    # spec = importlib.util.spec_from_file_location("module.name", modelpath)
    # model = importlib.util.module_from_spec(spec)

    return model


def train(model, trainsettings, traindata_gen, num_trainsamples, validdata_gen, num_validsamples, start_from_epoch):
    # todo check the possibility to use nb_workers > 1 with threadsafe generators https://github.com/fchollet/keras/issues/1638
    tvh = model.fit_generator(generator=traindata_gen,
                              samples_per_epoch=num_trainsamples,
                              validation_data=validdata_gen,
                              nb_val_samples=num_validsamples,
                              initial_epoch=start_from_epoch,
                              **trainsettings)
    return tvh


def validate(model, validdata_gen, num_validsamples):
    vprobs, vclass, vtrues = _predict(model, validdata_gen, nosamples=num_validsamples)
    return vprobs, vclass, vtrues


def test(model, testdata_gen, num_testsamples):
    tprobs, tclass, ttrues = _predict(model, testdata_gen, nosamples=num_testsamples)
    return tprobs, tclass, ttrues


def _predict(model, datagenerator, nosamples):
    """Returns:
        pprobs - numpy array of predictions [[0.8000, 0.2000], [0.6500, 0.3500]]
        pclass - numpy array of predicted classes [0 0 1 0 0 1 1 ...]
        ptrues - list of true labels [0 0 ...]   """

    pprobs, ptrues = [], []

    for i in range(nosamples):
        x, y = next(datagenerator)
        prob = model.predict(x, batch_size=1, verbose=0)

        pprobs.append(prob[0])
        ptrues.append(y[0])

    pclass = categorical_probas_to_classes(pprobs)
    ptrues = categorical_probas_to_classes(ptrues)

    return pprobs, pclass, ptrues


def create_model_folder(path_to_exp_folder, model_id):
    path_to_mod_folder = os.path.join(path_to_exp_folder, 'Model-{0}'.format(model_id))
    if not os.path.exists(path_to_mod_folder):
        os.makedirs(path_to_mod_folder)
    return path_to_mod_folder


def save_experiment_code(path_to_mod_folder, model_id, parameters):
    """Makes a copy of model source code to its experiment model folder."""
    src = '{0}/model.py'.format(os.getcwd())
    dst = '{0}/{1}_exp_code.py'.format(path_to_mod_folder, model_id)
    copyfile(src, dst)

    # Adds reference of used parameters
    with open(dst, "a+") as fl:
        fl.write('\n\n{0}\n###   Used parameters: {1}\n{0}'.format(('#' * 100), parameters))


def save_model_summary(model, path_to_mod_folder, model_id):
    """Saves description of model layers and parameters to a text file."""
    summarypath = '{0}/{1}_mod_summ.txt'.format(path_to_mod_folder, model_id)
    originalstdout = sys.stdout
    sys.stdout = open(summarypath, 'w')
    print(model.summary())
    sys.stdout = originalstdout


def save_model_plot(model, path_to_mod_folder, model_id):
    """Plots model architecture and saves it to png."""
    plotpath = '{0}/{1}-mod_plot.png'.format(path_to_mod_folder, model_id)
    kplot(model, to_file=plotpath, show_shapes=True)


def save_model(model, path_to_mod_folder, model_id, epoch=False, last_only=True):
    """Saves the model architecture & weights to h5 file on user call or on callback call."""

    # todo remove deprecated
    # if epoch is not False:
    #     if last_only:
    #         delete_unnecessary_models(path_to_mod_folder)
    # else:
    #     epoch = 'arch'

    if last_only:
        delete_unnecessary_models(path_to_mod_folder)

    modelpath = '{0}/{1}-mod_{2}.h5'.format(path_to_mod_folder, model_id, epoch)
    model.save(modelpath, overwrite=True)


def load_saved_model(path_to_mod_folder, model_id, epoch=None, best=False):
    """Loads the model architecture & weights from h5 file for resuming of training or model usage."""
    if epoch is None and not best: raise ValueError('Epoch parameter of load_saved_modul() must be specified unless best=True.')
    if best: epoch = 'best'
    else: epoch = epoch -1
    modelpath = '{0}/{1}-mod_{2}.h5'.format(path_to_mod_folder, model_id, epoch)
    return load_model(modelpath)


def set_saving_of_last_model(trainsettings, path_to_mod_folder, model_id, model):
    save_last_model_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: save_model(model, path_to_mod_folder, model_id, epoch=epoch, last_only=True))
    trainsettings['callbacks'].append(save_last_model_callback)


def set_saving_of_best_model(trainsettings, path_to_mod_folder, model_id, monitor='val_acc'):
    """Saves the best model architecture & weights to h5 file during training by adding ModelCheckpoint callback."""
    modelpath = '{0}/{1}-mod_best.h5'.format(path_to_mod_folder, model_id)
    if not any([isinstance(x, ModelCheckpoint) for x in trainsettings['callbacks']]):
        model_checkpoint = ModelCheckpoint(
            filepath=modelpath,
            monitor=monitor,
            verbose=0,
            save_best_only=True,
            mode='auto')
        trainsettings['callbacks'].append(model_checkpoint)


class RunningHistory(Callback):
    """Custom callback for saving training history during training. Useful to recover from a crash."""
    def __init__(self, filepath, initial_run_hist=None):
        super(RunningHistory, self).__init__()
        self.filepath = filepath
        self.initial_run_hist = initial_run_hist

    def on_train_begin(self, logs={}):
        if self.initial_run_hist is None:
            self.run_hist = {'loss': [], 'acc': [], 'val_loss': [], 'val_acc': [], 'epoch': 0}
        else:
            self.run_hist = self.initial_run_hist

    def on_epoch_end(self, epoch, logs={}):
        self.run_hist['loss'].append(logs.get('loss'))
        self.run_hist['acc'].append(logs.get('acc'))
        self.run_hist['val_loss'].append(logs.get('val_loss'))
        self.run_hist['val_acc'].append(logs.get('val_acc'))
        self.run_hist['epoch'] = epoch
        self.save_run_hist()

    def save_run_hist(self):
        with open(self.filepath, "w+") as fl:
            rh = _convert_hist_to_float(self.run_hist)
            json.dump(rh, fl)


def set_returning_running_history(trainsettings, path_to_mod_folder, model_id, initial_run_hist=None):
    """Serializes Running History object for recovering purposes."""
    run_hist_path = '{0}/{1}-run_hist.json'.format(path_to_mod_folder, model_id)
    if not any([isinstance(x, RunningHistory) for x in trainsettings['callbacks']]):
        run_hist_callback = RunningHistory(
            filepath=run_hist_path,
            initial_run_hist=initial_run_hist)
        trainsettings['callbacks'].append(run_hist_callback)


def delete_unnecessary_models(path_to_mod_folder):
    """Deletes all unnecessary saved model.h5 that are not from the best epoch."""
    previous_models = [x for x in os.listdir(path_to_mod_folder) if 'h5' in x and 'best' not in x]
    [os.remove(os.path.join(path_to_mod_folder, previous)) for previous in previous_models]


# todo remove unused
def delete_unnecessary_run_hists(path_to_mod_folder):
    """Deletes unnecessary xxxxx-run_hist.json files."""
    run_hist_files = [x for x in os.listdir(path_to_mod_folder) if '-run_hist.json' in x]
    [os.remove(os.path.join(path_to_mod_folder, ruh_hist)) for ruh_hist in run_hist_files]


def _convert_hist_to_float(hist_dict):
    for key, value in hist_dict.items():
        if type(value) is list:
            hist_dict[key] = list(map(float, value))
    return hist_dict


def get_train_stats_dict(tvh, start_time, end_time, path_to_mod_folder, model_id, data_name):
    """Dictionary that keeps track of important auxiliar values."""
    trainstats = {}

    trainstats['data_name'] = data_name
    trainstats['model_id'] = model_id
    trainstats['path_to_mod_folder'] = path_to_mod_folder

    trainstats['tvh_results'] = tvh

    total_epochs = len(tvh['loss'])
    trainstats['total_epochs'] = total_epochs

    best_epoch = tvh['val_loss'].index(min(tvh['val_loss'])) + 1
    trainstats['best_epoch'] = best_epoch

    elapsed_time = end_time - start_time
    trainstats['elapsed_time'] = elapsed_time

    # todo this does not work if model is resumed, total_epochs might be 1 and start from epoch might be 99
    one_epoch_duration = elapsed_time / (total_epochs - start_from_epoch)
    trainstats['one_epoch_duration'] = one_epoch_duration

    total_time_s = total_epochs * one_epoch_duration
    trainstats['total_time_s'] = total_time_s

    total_time_m = total_time_s / 60
    trainstats['total_time_m'] = total_time_m

    best_epoch_time_m = total_time_m / (total_epochs / best_epoch)
    trainstats['best_epoch_time_m'] = best_epoch_time_m

    return trainstats


def save_tvacc_plot(trainstats):
    """Plots training accuracy and validation accuracy over epoch number."""
    results = trainstats['tvh_results']
    plotpath = '{0}/{1}-tva_plot.pdf'.format(trainstats['path_to_mod_folder'], trainstats['model_id'])
    accTYaxis = results['acc']
    accVYaxis = results['val_acc']
    epochXaxis = list(range(1, len(results['acc']) + 1))
    plt.plot(epochXaxis, accTYaxis, 'b-', label='train acc.', linewidth=1.0)
    plt.plot(epochXaxis, accVYaxis, 'r-', label='valid acc.', linewidth=2.0)
    plt.ylabel('accuracy')
    plt.xlabel('no. of epochs')
    plt.legend(loc='upper left')
    plt.axis([1, len(results['acc']), 0, 1])
    plt.title('Training and Validation accuracy of model {0}'.format(trainstats['model_id']))
    plt.grid(True)
    plt.savefig(plotpath)


# todo remove deprecated, now history is being saved at each epoch end
def save_tvhistory_json(trainstats):
    """Save history of training and validation to a file."""
    results = trainstats['tvh_results']
    results = _convert_hist_to_float(results)
    histpath = '{0}/{1}-tvh_dict.json'.format(trainstats['path_to_mod_folder'], trainstats['model_id'])
    with open(histpath, "a+") as fl:
        json.dump(results, fl)


def load_run_hist_json(path_to_mod_folder, model_id):
    """Loads train-valid-history dictionary from json."""
    tvhistpath = '{0}/{1}-run_hist.json'.format(path_to_mod_folder, model_id)
    with open(tvhistpath, 'r') as fl:
        return json.load(fl)


def save_model_results(trainstats, list_of_class_names, vclass, vtrues, tclass=None, ttrues=None):
    """Saving tvresults, classification report, confusion matrices to a file."""

    tvlogpath = '{0}/{1}_tvh_log.txt'.format(trainstats['path_to_mod_folder'], trainstats['model_id'])

    # Redirecting stdout to a file to utilize print methods
    originalstdout = sys.stdout
    sys.stdout = open(tvlogpath, 'a+')

    print_tvresults(trainstats)

    # Saving VALIDATION classification report and confusion matrix
    print_classification_report(vtrues, vclass, list_of_class_names, name='VALIDATION')
    print_confusion_matrix(vtrues, vclass, list_of_class_names, name='VALIDATION')

    # Saving TESTING classification report and confusion matrix, if data is provided
    if not any([tclass is None, ttrues is None, list_of_class_names is None]):
        print_classification_report(ttrues, tclass, list_of_class_names, name='TESTING')
        print_confusion_matrix(ttrues, tclass, list_of_class_names, name='TESTING')

    # Redirecting stdout back to console
    sys.stdout = originalstdout


def save_data_info(trainstats, num_train, num_valid, num_test):
    tvlogpath = '{0}/{1}_tvh_log.txt'.format(trainstats['path_to_mod_folder'], trainstats['model_id'])
    with open(tvlogpath, 'a+') as fl:
        print('DATA USED: {0}\n'.format(trainstats['data_name']), file=fl)
        print('\tNo.train: {0} No.valid: {1} No.test: {2}'.format(num_train, num_valid, num_test), file=fl)


def print_tvresults(trainstats):
    """Prints results of training and validation to console."""
    ts = trainstats
    results = trainstats['tvh_results']
    b_e = trainstats['best_epoch']

    print('\nMODEL {0} - RESULTS OF TRAINING AND VALIDATION'.format(ts['model_id']))
    print('\tTraining of {0} epochs took {1:.2f} minutes. Best epoch = {2}. Training took {3:.2f} minutes'
          .format(ts['total_epochs'], ts['total_time_m'], b_e, ts['best_epoch_time_m']))
    print('\tTrain acc: \t{0:.2f}%'.format(results['acc'][b_e - 1] * 100))
    print('\tTrain loss:\t{0:.2f}%'.format(results['loss'][b_e - 1] * 100))
    print('\tValid acc: \t{0:.2f}%'.format(results['val_acc'][b_e - 1] * 100))
    # Coloring to console '\033[92m' - green '\033[0m' - end of color
    print('\tValid loss:\t{0:.2f}%'.format(results['val_loss'][b_e - 1] * 100))
    print()


def save_train_epoch_log(trainstats):
    """Saving the train log with results in each epoch, best epoch is denoted with ***."""
    results = trainstats['tvh_results']
    tvlogpath = '{0}/{1}_tvh_log.txt'.format(trainstats['path_to_mod_folder'], trainstats['model_id'])
    digits = len(str(trainstats['total_epochs']))

    # todo compute the time based on restarted time
    with open(tvlogpath, 'a+') as fl:
        print('\nTRAINING LOG:', file=fl)
        for epoch, value in enumerate(zip(results['loss'], results['acc'], results['val_loss'], results['val_acc'])):
            best = '***' if epoch + 1 == trainstats['best_epoch'] else ''  # Denotes best epoch in log with three stars
            print('{0:.1f}s |'.format(trainstats['one_epoch_duration']),
                  'Epoch {0:0>{digits}} /'.format(epoch + 1, digits=digits),
                  '{0:0>{digits}}: '.format(trainstats['total_epochs'], digits=digits),
                  'tl:{0:.4f}\tta:{1:.4f}\tvl:{2:.4f}\tva:{3:.4f} \t{4}'
                  .format(value[0], value[1], value[2], value[3], best), file=fl)


def save_file_result_log(trainstats, vprobs, vclass, vtrues, tprobs, tclass, ttrues, list_of_valid, list_of_test):
    """Saves log of results for each file in validation and / or testing set."""
    filelogpath = '{0}/{1}_res_log.txt'.format(trainstats['path_to_mod_folder'], trainstats['model_id'])
    val = [list_of_valid, vtrues, vclass, vprobs, 'VALIDATION']
    test = [list_of_test, ttrues, tclass, tprobs, 'TESTING']

    with open(filelogpath, 'a+') as fl:

        def gen_file_results_log(lof, ftrue, fpred, fprob, name):
            if lof is not None:
                print('\n{0} FILE PREDICTIONS wth confidence:'.format(name), file=fl)
                [print('{0}\ttrue: {1}\tpred: {2}, \tprob: {3}, \t name: {4}'.format(true == pred, true, pred, prob, name), file=fl)
                 for true, pred, prob, name in zip(ftrue, fpred, fprob, lof)]

        for x in [val, test]:
            gen_file_results_log(lof=x[0], ftrue=x[1], fpred=x[2], fprob=x[3], name=x[4])


def print_classification_report(y_true, y_predicted, list_of_class_names, name='VALIDATION'):
    """Prints the classification report for input data to console if any."""
    if any([y_true is None, y_predicted is None, list_of_class_names is None]):
        print('\nNo data was provided for creating {0} classification report.'.format(name))
    else:
        print('\n{0} | CLASSIFICATION REPORT:'.format(name))
        print(classification_report(y_true, y_predicted, target_names=list_of_class_names))


def print_confusion_matrix(y_true, y_predicted, list_of_class_names, name='VALIDATION'):
    """Prints the confusion matrix for input data to console if any."""
    if any([y_true is None, y_predicted is None, list_of_class_names is None]):
        print('\nNo data was provided for creating {0} confusion matrix.'.format(name))
    else:
        # Creating the confusion matrix using sk_learn library
        confmat = confusion_matrix(y_true, y_predicted)

        # Creating pretty ASCII table formatting for confusion matrix
        tab = tt.Texttable()

        tab.header(['CONFUSION MATRIX'] +
                   ['true: {0}'.format(list_of_class_names[i]) for i in range(len(confmat[0]))] +
                   ['class recall'])

        lastrow = ['class precision:']
        diagonal = 0
        for i, valrow in enumerate(confmat):
            tab.add_row(['pred.: {0}'.format(list_of_class_names[i])] + list(valrow) + [
                '{0: .2f} %'.format((valrow[i] / sum(valrow)) * 100)])
            lastrow.append(
                '{0: .2f} %'.format((valrow[i] / sum([confmat[j][i] for j in range(len(confmat[0]))])) * 100))
            diagonal += valrow[i]
        accuracy = diagonal / sum([sum(confmat[i]) for i in range(len(confmat[0]))])
        lastrow.append('acc: {0: .2f} %'.format(accuracy * 100))
        tab.add_row(lastrow)
        tab.set_deco(tab.HEADER | tab.VLINES | tab.HLINES)
        confmatascii = tab.draw()

        # Printing the ASCII table to console
        print('\n{0} | CONFUSION MATRIX:'.format(name))
        print(confmatascii)
        print()


def rename_model_folder(trainstats):
    """Renames the model folder after the experiment so it starts with validation accuracy value."""
    results = trainstats['tvh_results']
    model_folder_name = os.path.split(trainstats['path_to_mod_folder'])[-1]

    best_epoch = results['val_loss'].index(min(results['val_loss'])) + 1
    acc_t = results['acc'][best_epoch - 1] * 100
    acc_v = results['val_acc'][best_epoch - 1] * 100

    src = trainstats['path_to_mod_folder']
    dst = os.path.join(path_to_exp_folder, 'V{0:.2f}-T{1:.2f}-BE{2}_{3}'
                       .format(acc_v, acc_t, best_epoch, model_folder_name))
    os.rename(src, dst)


# todo add verbose parameter for monitoring progress of training in console
def run(path_to_exp_folder, exp_id, param_id, GPU_core, start_from_epoch):
    np.random.seed(2004)

    # Get data provided by datagenerator.py
    traindata_gen, validdata_gen, testdata_gen, \
    num_trainsamples, num_validsamples, num_testsamples, list_of_class_names, \
    list_of_train, list_of_valid, list_of_test, data_name = data_provide()

    # Load user defined model with all necessary parameter sets
    md = loadmodelmodul(modelpath=os.path.join(os.getcwd(), 'model.py'))

    parameters = md.tryparameters[param_id]
    model_id = param_id + 1000  # +1000 so the id is always at least 4 digits
    path_to_mod_folder = create_model_folder(path_to_exp_folder, model_id)

    # Starting training from scratch, compiling new model and saving
    if start_from_epoch == 0:
        np.random.seed(2004)
        model, trainsettings = md.create(compile=True, **parameters)
        initial_run_hist = None

        # Saving the model code of current experiment with appended parameters that were used to train the model
        save_experiment_code(path_to_mod_folder, model_id, parameters)

        # Saving the text summary of model architecture
        save_model_summary(model, path_to_mod_folder, model_id)

        # Saving the plot of model architecture
        save_model_plot(model, path_to_mod_folder, model_id)

    # Restarting training of saved model at desired epoch
    else:
        model = load_saved_model(path_to_mod_folder, model_id, start_from_epoch, best=False)
        trainsettings = md.create(compile=False, **parameters)
        # Loading previous training history from json
        initial_run_hist = load_run_hist_json(path_to_mod_folder, model_id)

    # Setting the callbacks for saving the last and the best model architecture & weights during training
    set_saving_of_last_model(trainsettings, path_to_mod_folder, model_id, model)
    set_saving_of_best_model(trainsettings, path_to_mod_folder, model_id, monitor='val_loss')
    # Setting the callback for saving the trainvalid history at each epoch
    set_returning_running_history(trainsettings, path_to_mod_folder, model_id, initial_run_hist=initial_run_hist)

    # print('\n{0}{1}\nExperiment {2} | Model {3}\nParameters: {4}\n{1}{5}\n'
    #       .format('\033[94m', '#' * 80, exp_id, model_id, parameters, '\033[0m'))

    if traindata_gen is not None:
        # sys.stdout.write('\r' + 'TRAINING')
        train_start_time = time.time()
        np.random.seed(2004)
        hist = train(model, trainsettings, traindata_gen, num_trainsamples, validdata_gen, num_validsamples,
                    start_from_epoch)
        train_end_time = time.time()
        # sys.stdout.write('\r' + 'VALIDATING')

        # Loading best model for validation and testing
        model = load_saved_model(path_to_mod_folder, model_id, epoch=None, best=True)
        vprobs, vclass, vtrues = validate(model, validdata_gen, num_validsamples)
    else:
        raise ValueError(
            'The variable train_valid in datagenerator.py function data_provide() must be set to True for now.')

    if testdata_gen is not None:
        # sys.stdout.write('\r' + 'TESTING')
        tprobs, tclass, ttrues = test(model, testdata_gen, num_testsamples)
        # sys.stdout.write('\r' + 'DONE: Training | Validating | Testing')
    else:
        tprobs = tclass = ttrues = None
        # sys.stdout.write('\r' + 'DONE: Training | Validating')

    # Explicitly choosing which history use for saving the results
    if start_from_epoch == 0:
        tvh = hist.history  # This contains only history from last resume
    else:
        tvh = load_run_hist_json(path_to_mod_folder, model_id)

    # Computing auxiliar stats about training
    trainstats = get_train_stats_dict(tvh, train_start_time, train_end_time, path_to_mod_folder, model_id, data_name)

    # todo remove deprecated, history is being saved and overwritten at each epoch end
    # Saving the history of training and validation
    save_tvhistory_json(trainstats)

    # Saving the plot of train and valid accuracy over epoch number
    save_tvacc_plot(trainstats)

    # Saving information about data
    save_data_info(trainstats, num_trainsamples, num_validsamples, num_testsamples)

    # Saving train & validation log + classification reports and confusion matrices
    save_model_results(trainstats, list_of_class_names, vclass, vtrues, tclass, ttrues)

    # Saving epoch log
    save_train_epoch_log(trainstats)

    # Saving log of results for validation and testing files
    save_file_result_log(trainstats, vprobs, vclass, vtrues, tprobs, tclass, ttrues, list_of_valid, list_of_test)

    print('\n{0}{1}\nDONE using GPU core: {2} | Experiment {3} | Model {4}\nParameters: {5}\n{1}{6}'
          .format('\033[94m', '#' * 80, GPU_core, exp_id, model_id, parameters, '\033[0m'))

    # Printing the train & validation accuracies
    print_tvresults(trainstats)

    # Renaming model folder to show Val_acc, Train_acc and best epoch number
    rename_model_folder(trainstats)


if __name__ == '__main__':
    # Code that takes arguments from subprocess.call and bootstraps the execution
    args = sys.argv
    path_to_exp_folder = args[1]
    exp_id = int(args[2])
    param_id = int(args[3])
    GPU_core = int(args[4])
    start_from_epoch = int(args[5])
    run(path_to_exp_folder, exp_id, param_id, GPU_core, start_from_epoch)
