##############################################################################
# This code creates an Exp/Exp-00000 folder for saving the results of models to be run.
# It creates worker in different thread for each of your GPU cores.
# Than puts a list of parameter ids to try into a queue.
# Each worker executes a model on its GPU core until the queue is empty.
##############################################################################

import time
import random
import os, sys
from queue import Queue
from shutil import copyfile
from subprocess import call
from threading import Thread
from model import num_of_GPU_cores, tryparameters


def bootstrap_the_experiment(resume=False, exp_id_toresume=None):


    def create_experiment_folder():
        while True:
            # Using random from built-in library instead of numpy so we do not mess up our numpy.random.seed()
            # This gives us the same results for the same model run multiple times within differene experiment ids.

            exp_id = random.randint(10000, 99999)
            path_to_exp_folder = os.path.join(os.getcwd(), 'Exp/Exp-{0}'.format(exp_id))
            if not os.path.exists(path_to_exp_folder):
                os.makedirs(path_to_exp_folder)
                return path_to_exp_folder, exp_id


    def copy_resume_code_to_root(path_to_code):
        print('To resume the training of older experiment, the original model code needs to be copied to root of the project.'
              'Any current "model.py" is backed up in "old_model.py"')
        path_to_root_code = os.path.join(os.getcwd(), 'model.py')
        os.rename(path_to_root_code, os.path.join(os.getcwd(), 'old_model.py'))
        copyfile(path_to_code, path_to_root_code)

        # Read the content of model code and delete last 3 lines
        with open(path_to_root_code, 'r+') as file:
            lines = file.readlines()
            lines = lines[:-3]
            file.writelines(lines)

        # Overwrite the file with the remaining lines
        with open(path_to_root_code, 'w') as file:
            [file.write(line) for line in lines]


    def model_runner(q, GPU_core):
        while True:
            param_set_id, start_from_epoch = q.get()
            q.task_done()

            # Assigning work to specific GPU core on Linux
            # todo check if this works also on distributions other than Ubuntu
            if sys.platform == 'linux':
                os.environ["THEANO_FLAGS"] = "device=gpu{0}".format(GPU_core)

            # Assigning work to specific GPU core on Windows
            elif sys.platform == 'windows':
                import theano.sandbox.cuda
                theano.sandbox.cuda.use('gpu{0}'.format(GPU_core))
            else:
                raise EnvironmentError('It seems your platform {0} is not yet supported. Please consider contributing.'.format(sys.platform))

            code = call(["python3",
                         "experiment.py",
                         "{}".format(path_to_exp_folder),
                         "{}".format(exp_id),
                         "{}".format(param_set_id),
                         "{}".format(GPU_core),
                         "{}".format(start_from_epoch)])
            exitcodes.append(code)


    exp_start_time = time.time()
    num_threads = num_of_GPU_cores
    num_param_sets = len(tryparameters)
    q = Queue(maxsize=0)
    exitcodes = []

    if resume:
        # Set path to experiment to experiment folder which is to be resumed
        exp_id = exp_id_toresume
        path_to_exp_folder = os.path.join(os.getcwd(), 'Exp', 'Exp-{0}'.format(exp_id))

        # Finds the indices of parameter sets that has already been finished or failed for example for memory error
        already_finished = [x for x in os.listdir(path_to_exp_folder) if '_' in x]
        already_finished_indices = [int(x[-4:])-1000 for x in already_finished]
        already_failed_indices = [int(x[-4:])-1000 for x in os.listdir(path_to_exp_folder) if len(os.listdir(os.path.join(path_to_exp_folder, x))) == 0]
        already_started = [x for x in os.listdir(path_to_exp_folder) if '_' not in x and len(os.listdir(os.path.join(path_to_exp_folder, x))) > 0]
        already_started_indices = [int(x[-4:])-1000 for x in already_started]

        # Finds epoch numbers from which to restart those models that need to be restarted
        to_restart = []
        for modelname in already_started:
            last_models = [x for x in os.listdir(os.path.join(path_to_exp_folder, modelname)) if 'h5' in x and 'best' not in x]
            idx = int(modelname[-4:])-1000
            if last_models:
                last_model_h5 = last_models[0]
                last_finished_epoch = int(last_model_h5[9:-3])
                to_restart.append((idx, last_finished_epoch + 1))

        if len(already_started) > 0:
            model_code_path = os.path.join(path_to_exp_folder, already_started[0], '{0}_exp_code.py'.format(already_started[0][-4:]))
        elif len(already_finished) > 0:
            model_code_path = os.path.join(path_to_exp_folder, already_finished[0], '{0}_exp_code.py'.format(already_finished[0][-4:]))
        else:
            raise EnvironmentError('It seems there is no model code from which to resume the experiment. '
                                   '\nCheck if you did not delete xxxx_exp_code.py from Model folders.')

        copy_resume_code_to_root(model_code_path)

    else:
        # Create new Experiment folder for results of models
        path_to_exp_folder, exp_id = create_experiment_folder()
        already_finished_indices, already_failed_indices, already_started_indices, to_restart = [], [], [], []
        # model_code_path = os.path.join(os.getcwd(), 'model.py')


    # Running workers on different threads for each GPU core
    for GPU_core in range(num_threads):
        worker = Thread(target=model_runner, args=(q, GPU_core))
        worker.setDaemon(True)
        worker.start()

    # Creating list of indices of parameter sets that should start from 0 epoch
    param_set_indices = [(x, 0) for x in range(num_param_sets)
                         if x not in already_finished_indices
                         and x not in already_failed_indices
                         and x not in already_started_indices]

    # Concatenating the list of parameter sets to restart and to start from the begining
    param_set_indices = to_restart + param_set_indices
    for param_set_id in param_set_indices:
        q.put(param_set_id)

    # Waiting for the queue to get empty
    q.join()

    # After the queue gets empty, we still want to wait for all threads to finish (we just count exit codes)
    while (len(exitcodes) < len(param_set_indices)):
        time.sleep(1)

    num_successfull = exitcodes.count(0)
    num_failed = len(exitcodes) - num_successfull
    exp_elapsed_time_minutes = (time.time() - exp_start_time) / 60

    def brief_report():
        return '{5}\nALL {0} EXPERIMENTS DONE IN {1:.2f} MINUTES ON {2} GPU CORES\n{3} SUCCESSFUL | {4} FAILED\n{5}'\
            .format(num_param_sets, exp_elapsed_time_minutes, num_of_GPU_cores, num_successfull, num_failed, '#' * 80)

    # Printing brief report to console in green.
    print('\033[92m' + brief_report() + '\033[0m')

    # Saving brief report to a file in exp_folder
    with open(os.path.join(path_to_exp_folder, 'brief_report.txt'), 'a+') as file:
        file.write(brief_report())

if __name__ == "__main__":
    # If you want to run the experiment from the scratch use this call:
    bootstrap_the_experiment()

    # If you want to restart unfinished experiment, set resume to True and exp_id_toresume:
    # bootstrap_the_experiment(resume=True, exp_id_toresume=12345)

